// Hud.h
// Christopher Ball 2016
// class for all hud elements like 
// buttons and notifications

#ifndef _HUD_H_
#define _HUD_H_

#include "pch.h"
#include "GameObject.h"

// forward declarations
class Graphics;
class Texture;
class Sprite;
class Button;
class Popup;

class Hud
{
public:
	Hud();
	~Hud();
	void			Init(Graphics* graphics); // initialise hud
	void			Update(float deltaTime);
	void			Render();			// render hud
			
	void			ReleaseAll();		// release all sprites
	void			DeleteAll();		// delete pointers
	void			Reset();			// reset hud on game reset

	void			SetTimer(unsigned int minutes, unsigned int seconds); // set game timer
	void			SetCounter(unsigned int counter); // set match count
	void			SetAttempts(unsigned int attempts); // set attempts made so far
	
	std::vector<GameObject*> GetObjects() const { return m_clickables; } // return object list

	void			TogglePopup(const wchar_t* ID, bool active);
	void			ShowPlayAgain();

private:
	Graphics*		m_graphics;			// graphics pointer
	
	SpriteFont*		m_pixelmixFont;		// sprite font pointer

	Texture*		m_titleTexture;		// title texture
	Texture*		m_buttonTexture;	// hud texture
	Texture*		m_quitButtonTexture;// quit button texture
	Texture*		m_playagainTexture; // play again button texture
	Texture*		m_matchTexture;		// match notification texture
	Texture*		m_timesupTexture;	// time's up notification texture
	Texture*		m_winTexture;		// win texture

	Sprite*			m_titleSprite;		// title sprite
	Sprite*			m_buttonSprite;		// hud sprite
	Sprite*			m_quitButtonSprite;	// quit button sprite
	Sprite*			m_playagainSprite;	// play again button sprite
	Sprite*			m_matchSprite;		// match notification sprite
	Sprite*			m_timesupSprite;	// time's up notification sprite
	Sprite*			m_winSprite;		// win sprite

	Button*			m_titleButton;		// title button
	Button*			m_timerButton;		// timer button
	Button*			m_counterButton;	// match counter button
	Button*			m_quitButton;		// quit button
	Button*			m_playagainButton;	// play again button

	Popup*			m_matchPopup;		// match popup notifictation
	Popup*			m_timesupPopup;		// time's up popup notification
	Popup*			m_winPopup;			// player won popup notification


	std::vector<GameObject*> m_clickables; // list of interactive objects

	std::vector<GameObject*> m_popups;	// list of popup notifications

	unsigned int	m_timerSeconds;		// game timer seconds
	unsigned int	m_timerMinutes;		// game timer minutes
	unsigned int	m_counter;			// number of matches left
	unsigned int	m_attempts;			// number of attempts to find a pair

	std::wstring	m_timerString;		// time string to display time left
	std::wstring	m_counterString;	// counter string to display matches left
	std::wstring	m_attemptsString;	// attempts string to display in hud

};

#endif _HUD_H_
