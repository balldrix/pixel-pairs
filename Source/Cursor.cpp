#include "Cursor.h"
#include "Sprite.h"

Cursor::Cursor() :
m_clickSound(nullptr),
m_clicked(false)
{}

Cursor::~Cursor()
{}

void
Cursor::Update(float deltaTime)
{
	m_sprite->SetPosition(m_position); // set position
	m_hitbox.SetAABB(Vector2(0.0f, 0.0f), Vector2(0.0f, 0.0f)); // set collision box
	m_hitbox.OffSetAABB(m_position); // offset hitbox

}

void
Cursor::Render(Graphics* graphics)
{
	// render cursor
	m_sprite->Render(graphics);
}

void
Cursor::SetSound(SoundEffect* sound)
{
	m_clickSound = sound;
}

void
Cursor::SetClicked(bool click)
{
	m_clicked = click;
}
