#include "Popup.h"
#include "Sprite.h"

Popup::Popup()
{}

Popup::~Popup()
{}

void
Popup::Update(float deltaTime)
{}

void
Popup::Render(Graphics* graphics)
{
	// render popup sprite
	m_sprite->Render(graphics, m_position);
}
