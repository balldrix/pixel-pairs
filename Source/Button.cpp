#include "Button.h"
#include "Graphics.h"
#include "Sprite.h"

Button::Button()
{}

Button::~Button()
{}

void	
Button::Update(float deltaTime)
{
}

void 
Button::Render(Graphics* graphics)
{
	// set sprite position and render
	m_sprite->SetPosition(m_position);
	m_sprite->Render(graphics);
}
