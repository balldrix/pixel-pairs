#include "GameplayGameState.h"
#include "GameStateManager.h"
#include "Graphics.h"
#include "Input.h"
#include "Texture.h"
#include "Sprite.h"
#include "Button.h"
#include "Cursor.h"
#include "Hud.h"
#include "Deck.h"
#include "Card.h"

GameplayGameState::GameplayGameState()
{}

GameplayGameState::GameplayGameState(GameStateManager* gameStateManager) :
m_gameStateManager(nullptr),
m_graphics(nullptr),
m_input(nullptr),
m_audio(nullptr),
m_gamePlayMusic(nullptr),
m_clickSound(nullptr),
m_flipCardSound(nullptr),
m_matchSound(nullptr),
m_timeupSound(nullptr),
m_youWinSound(nullptr),
m_backgroundTexture(nullptr),
m_cursorTexture(nullptr),
m_backgroundSprite(nullptr),
m_cursorSprite(nullptr),
m_mouseCursor(nullptr),
m_timer(0.0f),
m_turnTimer(0.0f),
m_timerSeconds(0),
m_timerMinutes(0),
m_matchesLeft(0),
m_attempts(0),
m_turnCounter(TURN_ONE),
m_firstPick(nullptr),
m_running(false),
GameState(L"GAMEPLAY")
{
	// get essential pointers from gamestate manager
	m_gameStateManager = gameStateManager;
	m_graphics = m_gameStateManager->GetGraphics();
	m_input = m_gameStateManager->GetInput();
	m_audio = m_gameStateManager->GetAudio();

	m_input->SetMouseClicked(false); // set mouse clicked to false
}

GameplayGameState::~GameplayGameState()
{
}

void
GameplayGameState::OnEntry()
{
	// load assets for the gameplay state
	LoadAssets();
}

void
GameplayGameState::OnExit()
{
	// release memory and delete assets
	ReleaseAll();
	DeleteAssets();
}

void
GameplayGameState::LoadAssets()
{
	// seed random generator
	srand((int)time(NULL));

	// load sounds
	m_clickSound = new SoundEffect(m_audio, L"Assets\\Sounds\\click.wav");
	m_flipCardSound = new SoundEffect(m_audio, L"Assets\\Sounds\\flipcard.wav");
	m_matchSound = new SoundEffect(m_audio, L"Assets\\Sounds\\match.wav");
	m_timeupSound = new SoundEffect(m_audio, L"Assets\\Sounds\\timesup.wav");
	m_youWinSound = new SoundEffect(m_audio, L"Assets\\Sounds\\youwin.wav");
	m_gamePlayMusic = new SoundEffect(m_audio, L"Assets\\Sounds\\gameplay.wav");

	// initialise background music
	m_backgroundLoop = m_gamePlayMusic->CreateInstance();
	m_backgroundLoop->Play(true); // play in a loop
	m_backgroundLoop->SetVolume(0.5f); // reduce volume a little

	// create texture memory
	m_backgroundTexture = new Texture();
	m_cursorTexture = new Texture();

	// create sprite memory
	m_backgroundSprite = new Sprite();
	m_cursorSprite = new Sprite();

	// create object memory
	m_mouseCursor = new Cursor();

	// load textures
	m_backgroundTexture->LoadTexture(m_graphics, "Assets//Textures//background.png");
	m_cursorTexture->LoadTexture(m_graphics, "Assets//Textures//cursor.png");

	// initalise sprites
	m_backgroundSprite->Init(m_backgroundTexture);
	m_cursorSprite->Init(m_cursorTexture);
	m_cursorSprite->SetOrigin(Vector2(0.0f, 0.0f)); // set origin of sprite to top left for cursor acuracy

	// initialise hud
	m_hud.Init(m_graphics);

	// initialise deck
	m_deck.Init(m_graphics);
	
	// calculate game board position
	Vector2 boardPos;
	boardPos.x = m_graphics->GetWidth() * BOARD_POSITION_X;
	boardPos.y = m_graphics->GetHeight() * BOARD_POSITION_Y;
	
	// call deal cards method to sort 
	// deck position on game board
	m_deck.DealCards(boardPos, BOARD_NUM_COLUMNS, BOARD_NUM_ROWS);

	// initialise cursor
	m_mouseCursor->Init(m_cursorSprite, Vector2((float)m_input->GetMouseX(), (float)m_input->GetMouseY())); // init
	m_mouseCursor->GetHitBox().SetAABB(Vector2(0.0f,0.0f), Vector2(0.0f,0.0f)); // cursor hitbox is only 1x1 pixel size
	m_mouseCursor->SetActive(true); // set active

	// ready for mouse click
	m_inputReady = true;

	// set matches left
	m_matchesLeft = TOTAL_MATCHES;

	// set turncounter
	m_turnCounter = TURN_ONE;

	// set 30 seconds
	m_timerSeconds = START_SECONDS;
	m_timerMinutes = START_MINUTES;

	// set first pick
	m_firstPick = nullptr;

	// set running to true
	m_running = true;
}

void
GameplayGameState::DeleteAssets()
{
	// delete all deck items
	m_deck.DeleteAll();

	// delete all hud items;
	m_hud.DeleteAll();
	
	// delete mouse cursor
	if(m_mouseCursor)
	{
		delete m_mouseCursor;
		m_mouseCursor = nullptr;
	}

	// delete mouse cursor sprite
	if(m_cursorSprite)
	{
		delete m_cursorSprite;
		m_cursorSprite = nullptr;
	}

	// delete background sprite
	if(m_backgroundSprite)
	{
		delete m_backgroundSprite;
		m_backgroundSprite = nullptr;
	}

	// delete mouse cursor texture
	if(m_cursorTexture)
	{
		delete m_cursorTexture;
		m_cursorTexture = nullptr;
	}

	// delete background texture
	if(m_backgroundTexture)
	{
		delete m_backgroundTexture;
		m_backgroundTexture = nullptr;
	}

	// delete you win sound
	if(m_youWinSound)
	{
		delete m_youWinSound;
		m_youWinSound = nullptr;
	}

	// delete time's up sound
	if(m_timeupSound)
	{
		delete m_timeupSound;
		m_timeupSound = nullptr;
	}

	// delete match sound
	if(m_matchSound)
	{
		delete m_matchSound;
		m_matchSound = nullptr;
	}

	// delete flip card sound
	if(m_flipCardSound)
	{
		delete m_flipCardSound;
		m_flipCardSound = nullptr;
	}

	// delete click sound
	if(m_clickSound)
	{
		delete m_clickSound;
		m_clickSound = nullptr;
	}

	// delete gameplay music
	if(m_gamePlayMusic)
	{
		delete m_gamePlayMusic;
		m_gamePlayMusic = nullptr;
	}
}

void
GameplayGameState::ProcessInput()
{
	// quit when escape key is pressed
	if(m_input->IsKeyDown(VK_ESCAPE))
	{
		PostQuitMessage(0);
	}

	// if mouse button is clicked
	if(m_input->GetMouseClicked())
	{
		m_mouseCursor->SetClicked(true); // set cursor to clicked
	}

	// toggle mouse click off when mouse button has been released
	if(!m_input->GetMouseClicked())
	{
		m_mouseCursor->SetClicked(false);
	}
}

void
GameplayGameState::Update(float deltaTime)
{
	// update cursor
	m_mouseCursor->SetPosition(Vector2((float)m_input->GetMouseX(), (float)m_input->GetMouseY()));
	m_mouseCursor->Update(deltaTime);
	
	// if input status is ready
	if(m_inputReady)
	{
		// check if mouse was clicked
		if(m_mouseCursor->IsClicked())
		{
			// copy hud object container
			std::vector<GameObject*> hudObjects = m_hud.GetObjects();

			// loop through hud objects
			for(unsigned int i = 0; i < hudObjects.size(); i++)
			{
				// check the object is active
				if(hudObjects[i]->IsActive())
				{
					if(m_mouseCursor->GetHitBox().Collision(hudObjects[i]->GetHitBox()))
					{				
						if(hudObjects[i]->GetID() == L"QUIT")
						{
							// switch to menu state
							m_gameStateManager->SwitchState(L"MENU");	
						}

						if(hudObjects[i]->GetID() == L"PLAYAGAIN")
						{
							// reset game
							ResetGame();
						}

						// reset input toggle
						m_inputReady = false;
						return;
					}
				}
			}

			// make sure the game is still running
			if(m_running)
			{
				// copy card list container 
				std::vector<Card*> cards = m_deck.GetCards();

				// loop through cards
				for(unsigned int i = 0; i < cards.size(); i++)
				{
					// check if card has not already been matched
					if(!cards[i]->IsMatched())
					{
						// if mouse cursor is over a card
						if(m_mouseCursor->GetHitBox().Collision(cards[i]->GetHitBox()))
						{
							// switch using turn counter
							switch(m_turnCounter)
							{
								// on turn 1
								case TURN_ONE:
									m_flipCardSound->Play(); // play flip sound
									cards[i]->SetActive(true); // set card to active to reveal face
									m_firstPick = cards[i]; // set first pick to point to card
									m_turnCounter = TURN_TWO; // set to turn two
									break;

								// on turn 2
								case TURN_TWO:
									// make sure the player cannot cheat by clicking the same card twice
									if(m_firstPick != cards[i])
									{
										m_flipCardSound->Play(); // play flip sound
										cards[i]->SetActive(true); // set card to active to reveal face
										m_turnCounter = MAX_TURNS; // set to max turns for turn delay
										m_attempts++; // increase attempts made to find pair

										// if player matches a pair
										if(cards[i]->GetCardFace() == m_firstPick->GetCardFace())
										{
											// set both cards to matched
											cards[i]->SetMatched(true);
											m_firstPick->SetMatched(true);

											// decrease number of matches left
											m_matchesLeft--;

											// if all matches are found
											if(m_matchesLeft > 0)
											{
												// play matched sound
												m_matchSound->Play();

												// show match popup
												m_hud.TogglePopup(L"MATCH", true);

												// add 30s to game timer
												m_timerSeconds += BONUS_TIME;
											}
										}
										break;
									}
							}
							m_inputReady = false; 	  // reset input toggle
							return;
						}
					}
				}
			}
		}
	}

	// make sure the game is still running
	if(m_running)
	{
		// after player has picked 2 cards
		if(m_turnCounter == MAX_TURNS)
		{
			// increase turn timer
			m_turnTimer += deltaTime;

			// if turntimer has reached TURN_TIME
			if(m_turnTimer > TURN_TIME)
			{
				// copy card list container 
				std::vector<Card*> cards = m_deck.GetCards();
			
				// loop through cards
				for(unsigned int i = 0; i < cards.size(); i++)
				{
					// check if card is not matched already
					if(!cards[i]->IsMatched())
					{	
						// turn card to inactive
						cards[i]->SetActive(false);

						// show match popup
						m_hud.TogglePopup(L"MATCH", false);
					}
				}

				// reset turn counter to turn one
				m_turnCounter = TURN_ONE;

				// reset turn timer
				m_turnTimer = 0.0f;
			}
		}

		// increase timer with delta time
		m_timer += deltaTime;

		// if timer is 1 second, reduce timer seconds
		if(m_timer > 1.0f)
		{
			// if seconds is 0
			if(m_timerSeconds == 0)
			{
				m_timerSeconds = 59; // set seconds to 59
			}
			
			// reduce number of seconds
			m_timerSeconds--;

			// reset timer
			m_timer = 0.0f;
		}

		// if seconds goes over 59 increase 
		// minutes
		if(m_timerSeconds > 59)
		{
			// increase minutes
			m_timerMinutes++;

			// set seconds
			m_timerSeconds -= 59;
		}

		// set timer in hud
		m_hud.SetTimer(m_timerMinutes, m_timerSeconds);

		// set num of matches left in hud display
		m_hud.SetCounter(m_matchesLeft);

		// set attempts in hud
		m_hud.SetAttempts(m_attempts);

		// if time is up
		if(m_timerSeconds == 0 && m_timerMinutes == 0)
		{
			// render time's up 
			m_hud.TogglePopup(L"TIMESUP", true);

			// stop music
			m_backgroundLoop->Stop();

			// play time's up sound
			m_timeupSound->Play();

			// show play again button
			m_hud.ShowPlayAgain();

			// set running to false
			m_running = false;
		}

		// if all matches are found
		if(m_matchesLeft == 0)
		{
			// play you win sound
			m_youWinSound->Play();

			// render you win 
			m_hud.TogglePopup(L"YOUWIN", true);

			// turn off match popup
			m_hud.TogglePopup(L"MATCH", false);

			// stop music
			m_backgroundLoop->Stop();

			// show play again button
			m_hud.ShowPlayAgain();

			// set running to false
			m_running = false;
		}
	}

	// only deal with mouse click if 
	// mouse button has been released
	if(!m_mouseCursor->IsClicked())
	{
		m_inputReady = true; // input is ready
	}
}

void
GameplayGameState::Render()
{
	//////////////////////////////////////
	// render background first
	if(m_backgroundSprite)
	{
		m_backgroundSprite->Render(m_graphics,
									Vector2(m_graphics->GetWidth() / 2,
									m_graphics->GetHeight() /2));
	}

	// render deck
	m_deck.Render();

	// render hud
	m_hud.Render();
	
	//////////////////////////////////////
	// render mouse cursor
	// always render last!
	if(m_mouseCursor->IsActive())
	{
		m_mouseCursor->Render(m_graphics);
	}
}

void
GameplayGameState::ReleaseAll()
{
	// release hud resources
	m_hud.ReleaseAll();

	// release deck resources
	m_deck.ReleaseAll();

	// release all texture resources
	if(m_cursorTexture) { m_cursorTexture->Release(); }
	if(m_backgroundTexture) { m_backgroundTexture->Release(); }
	
	// release background loop
	m_backgroundLoop.reset();
	m_audio->Reset();
}

void
GameplayGameState::ResetGame()
{
	// reset hud
	m_hud.Reset();

	// reset deck
	m_deck.Reset();

	// calculate game board position
	Vector2 boardPos;
	boardPos.x = m_graphics->GetWidth() * BOARD_POSITION_X;
	boardPos.y = m_graphics->GetHeight() * BOARD_POSITION_Y;

	// call deal cards method to sort 
	// deck position on game board
	m_deck.DealCards(boardPos, BOARD_NUM_COLUMNS, BOARD_NUM_ROWS);

	// set matches left
	m_matchesLeft = TOTAL_MATCHES;

	// reset number of attempts
	m_attempts = 0;

	// set turncounter
	m_turnCounter = TURN_ONE;

	// set 30 seconds
	m_timerSeconds = START_SECONDS;
	m_timerMinutes = START_MINUTES;

	// reset turn time
	m_turnTimer = 0.0f;

	// reset timer for game time
	m_timer = 0.0f;

	// set first pick
	m_firstPick = nullptr;

	// set running to true
	m_running = true;

	// play loop again
	m_backgroundLoop->Play(true);
}
