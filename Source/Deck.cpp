#include "Deck.h"

#include "Graphics.h"
#include "Texture.h"
#include "Sprite.h"
#include "Card.h"

Deck::Deck() :
m_cardFaceTexture(nullptr),
m_cardBackTexture(nullptr),
m_cardFaceSprite(nullptr),
m_cardBackSprite(nullptr)
{}

Deck::~Deck()
{
}

void
Deck::Init(Graphics* graphics)
{
	// copy graphics pointer
	m_graphics = graphics;

	// create new memory for textures
	m_cardFaceTexture = new Texture();
	m_cardBackTexture = new Texture();

	// create new memory for sprites
	m_cardFaceSprite = new Sprite();
	m_cardBackSprite = new Sprite();

	// load textures
	m_cardFaceTexture->LoadTexture(m_graphics, "Assets//Textures//cardSheet.png");
	m_cardBackTexture->LoadTexture(m_graphics, "Assets//Textures//cardBack.png");

	// initialise card face sprite
	m_cardFaceSprite->Init(m_cardFaceTexture);
	m_cardFaceSprite->SetOrigin(Vector2(CARD_WIDTH / 2, CARD_HEIGHT / 2));
	
	m_cardBackSprite->Init(m_cardBackTexture);

	// add cards to deck
	for(int i = 0; i < MAX_FACES; i++)
	{
		// loop twice so there are two of the same card face
		for(int j = 0; j < 2; j++)
		{
			// create new card in memory
			Card* card = new Card();

			// initialise card with spritesheet
			card->Init(m_cardFaceSprite);

			// set card face value
			card->SetFace((CardFace)i);

			// push two cards to the deck
			m_cardList.push_back(card);
		}
	}

	// shuffle deck
	ShuffleCards();
}

void 
Deck::Render()
{
	// loop through deck
	for(unsigned int i = 0; i < m_cardList.size(); i++)
	{
		// check if card is active
		if(m_cardList[i]->IsActive())
		{
			// set rect before rendering card
			// make new rect for the sprite sheet location
			RECT rect;
			rect.left = (m_cardList[i]->GetCardFace() % NUM_SHEET_COLUMNS) * CARD_WIDTH;
			rect.right = rect.left + CARD_WIDTH;
			rect.top = (m_cardList[i]->GetCardFace() / NUM_SHEET_COLUMNS) * CARD_HEIGHT;
			rect.bottom = rect.top + CARD_HEIGHT;

			// set rect for the sprite sheet
			m_cardList[i]->GetSprite()->SetRect(rect);

			// render card
			m_cardList[i]->Render(m_graphics);
		}
		else
		{
			// render back of card
			m_cardBackSprite->Render(m_graphics, m_cardList[i]->GetPosition());
		}
	}
}

void 
Deck::ReleaseAll()
{
	// release texture resources
	if(m_cardBackTexture) { m_cardBackTexture->Release(); }
	if(m_cardFaceTexture) { m_cardFaceTexture->Release(); }
}

void 
Deck::DeleteAll()
{
	// clear list of cards
	for(unsigned int i = 0; i < m_cardList.size(); i++)
	{
		Card* temp = m_cardList[i];
		delete temp;
		temp = nullptr;
	}
	m_cardList.clear();

	// delete card back sprite
	if(m_cardBackSprite)
	{
		delete m_cardBackSprite;
		m_cardBackSprite = nullptr;
	}

	// delete card sheet sprite
	if(m_cardFaceSprite)
	{
		delete m_cardFaceSprite;
		m_cardFaceSprite = nullptr;
	}

	// delete card back texture
	if(m_cardBackTexture)
	{
		delete m_cardBackTexture;
		m_cardBackTexture = nullptr;
	}

	// delete card spritesheet texture
	if(m_cardFaceTexture)
	{
		delete m_cardFaceTexture;
		m_cardFaceTexture = nullptr;
	}
}

void
Deck::Reset()
{
	// turn cards over
	for(unsigned int i = 0; i < m_cardList.size(); i++)
	{
		m_cardList[i]->SetActive(false); // set inactive
		m_cardList[i]->SetMatched(false); // set unmatched
	}

	// shuffle deck
	ShuffleCards();
}

void Deck::ShuffleCards()
{
	// shuffle deck using std::random_shuffle algorithm
	std::random_shuffle(m_cardList.begin(), m_cardList.end());
}

void
Deck::DealCards(Vector2 position, unsigned int columns, unsigned int rows)
{
	// loop through deck rows and columns
	for(unsigned int i = 0; i < rows; i++)
	{
		for(unsigned int j = 0; j < columns; j++)
		{
			Card* card = m_cardList[(i* columns) + j];
			
			// set new position of card
			card->SetPosition(Vector2(position.x + (j * CARD_WIDTH), position.y + (i * CARD_HEIGHT)));
			
			// set hitbox
			card->GetHitBox().SetAABB(Vector2(0.0f, 0.0f), Vector2((float)CARD_WIDTH, (float)CARD_HEIGHT));
			
			// set hitbox offset
			card->GetHitBox().OffSetAABB(
			card->GetPosition() - card->GetSprite()->GetOrigin());
		}
	}
}
