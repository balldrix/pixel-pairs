// Deck.h
// Christopher Ball 2016
// manages deck shuffle and display cards

#ifndef _DECK_H_
#define _DECK_H_

#include "pch.h"

// constants
const unsigned int DECK_SIZE = 24; // number of cards in deck
const unsigned int NUM_SHEET_COLUMNS = 4; // number of columns in card sprite sheet
const unsigned int NUM_SHEET_ROWS = 3; // number of rows in sprite sheet
const unsigned int CARD_WIDTH = 128; // width of card in pixels
const unsigned int CARD_HEIGHT = 128; // height of card in pixels

// forward declarations
class Graphics;
class Texture;
class Sprite;
class Card;

class Deck
{
public:
	Deck();
	~Deck();
	void				Init(Graphics* graphics);	// initialise deck
	void				Render();					// render cards 

	void				ReleaseAll();				// release all texture resources
	void				DeleteAll();				// delete all assets from memory
	void				Reset();					// reset deck when game is reset

	void				ShuffleCards();				// shuffle deck
	void				DealCards(Vector2 position, unsigned int columns, unsigned int rows); // set position to deal deck

	std::vector<Card*>	GetCards() const	{ return m_cardList; } // get card deck

private:
	Graphics*			m_graphics;					// pointer to graphics class
	Texture*			m_cardFaceTexture;			// cards spritesheet texture
	Texture*			m_cardBackTexture;			// card back texture
	
	Sprite*				m_cardFaceSprite;			// cards spritesheet
	Sprite*				m_cardBackSprite;			// card back sprite 
	
	std::vector<Card*>	m_cardList;						// container list of cards in deck
};

#endif _DECK_H_
