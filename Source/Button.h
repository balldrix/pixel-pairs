// Button.h
// Christopher Ball 2016
// UI button class

#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "GameObject.h"

// forward declarations
class Graphics;

class Button : public GameObject
{
public:
	Button();
	~Button();
	void Update(float deltaTime); // update button
	void Render(Graphics* graphics); // render button

private:
	// no private members needed
};

#endif _BUTTON_H_
