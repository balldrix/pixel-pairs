// GameplayGameState.h
// Christopher Ball 2016
// the game state for the main gameplay state

#ifndef _GAMEPLAYGAMESTATE_H_
#define _GAMEPLAYGAMESTATE_H_

#include "pch.h"
#include "GameState.h"
#include "Hud.h"
#include "Deck.h"

// forward declarations
class GameStateManager;
class Graphics;
class Input;
class Texture;
class Sprite;
class Button;
class Cursor;
class Card;

// constants
const float BOARD_POSITION_X = 0.4f; // board position in fraction of game width
const float BOARD_POSITION_Y = 0.25f; // board position in fraction of game height
const unsigned int BOARD_NUM_COLUMNS = 6; // number of columns in game board
const unsigned int BOARD_NUM_ROWS = 4; // number of rows in game board
const unsigned int TOTAL_MATCHES = 12; // matches to be found to win
const float TURN_TIME = 1.5f;	// time in between turns for clearing notifications
const unsigned int START_SECONDS = 30;  // time in seconds given at the start
const unsigned int START_MINUTES = 0;  // time in minutes given at the start
const unsigned int BONUS_TIME = 15; // bonus time given for a match

enum Turn
{
	TURN_ONE = 0,
	TURN_TWO,
	MAX_TURNS
};

class GameplayGameState : public GameState
{
public:
	GameplayGameState();
	GameplayGameState(GameStateManager* gameStateManager);
	~GameplayGameState();

	void				OnEntry();					// calls the load assets method
	void				OnExit();					// calls the delete assets method
	void				ProcessInput();				// processes input from player
	void				Update(float deltaTime);	// update state
	void				Render();					// render state
	void				ReleaseAll();				// release all texture memory

	void				ResetGame();				// reset game and play again
																
private:			
	void				LoadAssets();				// load game state assets
	void				DeleteAssets();				// delete game state assets

	GameStateManager*	m_gameStateManager;			// game state manager pointer
	Graphics*			m_graphics;					// graphics pointer
	Input*				m_input;					// input class pointer		
	AudioEngine*		m_audio;					// audio engine pointer

	std::unique_ptr<SoundEffectInstance> m_backgroundLoop;	// background loop
	SoundEffect*		m_gamePlayMusic;			// menu music
	SoundEffect*		m_clickSound;				// button click sound
	SoundEffect*		m_flipCardSound;			// flip card sound
	SoundEffect*		m_matchSound;				// match sound
	SoundEffect*		m_timeupSound;				// time's up sound
	SoundEffect*		m_youWinSound;				// you win sound
	
	Texture*			m_backgroundTexture;		// background texture
	Texture*			m_cursorTexture;			// custom cursor texture
	
	Sprite*				m_backgroundSprite;			// background sprite
	Sprite*				m_cursorSprite;				// custom cursor sprite	

	Hud					m_hud;						// pointer to hud
	Deck				m_deck;						// card deck

	Cursor*				m_mouseCursor;				// custom mouse cursor

	float				m_timer;					// game timer to game track time
	float				m_turnTimer;				// game turn timer
	unsigned int		m_timerSeconds;				// game timer seconds
	unsigned int		m_timerMinutes;				// game timer minutes
	unsigned int		m_matchesLeft;				// number of matches left to find
	unsigned int		m_attempts;					// number of attempts to find a pair

	Turn				m_turnCounter;				// turn indicator
	Card*				m_firstPick;				// pointer card picked in first turn

	bool				m_running;					// is game running or not
};

#endif _GAMEPLAYGAMESTATE_H_