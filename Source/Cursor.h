// Cursor.h
// Christopher Ball 2016
// mouse cursor class

#ifndef _CURSOR_H_
#define	_CURSOR_H_

#include "pch.h"
#include "GameObject.h"

class Cursor : public GameObject
{
public:
	Cursor();
	~Cursor();
	void			Update(float deltaTime); // update cursor
	void			Render(Graphics* graphics); // render cursor sprite
	
	void			SetSound(SoundEffect* sound); // set click sound
	void			SetClicked(bool click); // set clicked on or off

	bool			IsClicked() const { return m_clicked; } // return if clicked

	SoundEffect*	GetSound() const { return m_clickSound; } // return mouse click sound

private:
	SoundEffect*	m_clickSound; // sound of mouse click
	bool			m_clicked; // mouse clicked or not
};

#endif _CURSOR_H_