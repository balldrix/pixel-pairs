// Card.h
// Christopher Ball 2016
// Card class with front 
// and back faces

#ifndef _CARD_H_
#define _CARD_H_

#include "GameObject.h"

// forward declarations
class Graphics;

// enumeration for list of card faces
enum CardFace
{
	SONIC = 0,
	PICACHU,
	TOAD,
	JIM,
	MEGAMAN,
	PACMAN,
	BOMBERMAN,
	MARIO,
	YOSHI,
	CHUNLI,
	KEN,
	LINK,
	MAX_FACES
};

class Card : public GameObject
{
public:
	Card();
	~Card();

	void Update(float deltaTime);		// update card
	void Render(Graphics* graphics);	// render card
	void SetFace(CardFace face);		// set card face value
	void SetMatched(bool matched);		// set card to matched status

	CardFace GetCardFace() const { return m_cardFace; } // return card face value
	bool IsMatched() const { return m_matched; }		// return matched card status

private:
	CardFace	m_cardFace; // card face value
	bool		m_matched;	// has card been matched with it's pair
};

#endif _CARD_H_

