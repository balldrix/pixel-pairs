// Instructions.h
// Christopher Ball 2016
// instructions screen

#ifndef _INSTRUCTIONS_H_
#define _INSTRUCTIONS_H_

#include "GameObject.h"

// forward declarations
class Graphics;

class Instructions : public GameObject
{
public:
	Instructions();
	~Instructions();
	void Update(float deltaTime); // update instructions
	void Render(Graphics* graphics); // render instructions

private:
	// no private members needed
};

#endif _INSTRUCTIONS_H_
