#include "Hud.h"
#include "Graphics.h"
#include "Texture.h"
#include "Sprite.h"
#include "Button.h"
#include "Popup.h"

Hud::Hud() :
m_graphics(nullptr),
m_pixelmixFont(nullptr),
m_titleTexture(nullptr),
m_buttonTexture(nullptr),
m_quitButtonTexture(nullptr),
m_playagainTexture(nullptr),
m_matchTexture(nullptr),
m_timesupTexture(nullptr),
m_winTexture(nullptr),
m_titleSprite(nullptr),
m_buttonSprite(nullptr),
m_quitButtonSprite(nullptr),
m_playagainSprite(nullptr),
m_matchSprite(nullptr),
m_timesupSprite(nullptr),
m_winSprite(nullptr),
m_titleButton(nullptr),
m_timerButton(nullptr),
m_counterButton(nullptr),
m_quitButton(nullptr),
m_playagainButton(nullptr),
m_matchPopup(nullptr),
m_timesupPopup(nullptr),
m_winPopup(nullptr),
m_timerSeconds(0),
m_timerMinutes(0),
m_counter(0),
m_attempts(0),
m_timerString(L""),
m_counterString(L""),
m_attemptsString(L"")
{
}

Hud::~Hud()
{
}

void
Hud::Init(Graphics* graphics)
{	
	m_graphics = graphics; // copy graphics pointer
	
	// get middle of screen for popups
	Vector2 middle = Vector2(m_graphics->GetWidth() * 0.5f, m_graphics->GetHeight() * 0.5f);

	// initialise spritefont
	m_pixelmixFont = new SpriteFont(m_graphics->GetDevice(), L"Assets//Fonts//pixelmix24.spritefont");

	// create new texture memory
	m_titleTexture = new Texture();
	m_buttonTexture = new Texture();
	m_quitButtonTexture = new Texture();
	m_matchTexture = new Texture();
	m_timesupTexture = new Texture();
	m_winTexture = new Texture();
	m_playagainTexture = new Texture();

	// create new sprite memory
	m_titleSprite = new Sprite();
	m_buttonSprite = new Sprite();
	m_quitButtonSprite = new Sprite();
	m_matchSprite = new Sprite();
	m_timesupSprite = new Sprite();
	m_winSprite = new Sprite();
	m_playagainSprite = new Sprite();

	// load textures from file
	m_titleTexture->LoadTexture(m_graphics, "Assets//Textures//buttonTitle.png");
	m_buttonTexture->LoadTexture(m_graphics, "Assets//Textures//button.png");
	m_quitButtonTexture->LoadTexture(m_graphics, "Assets//Textures//buttonQuit.png");
	m_matchTexture->LoadTexture(m_graphics, "Assets//Textures//match.png");
	m_timesupTexture->LoadTexture(m_graphics, "Assets//Textures//timesup.png");
	m_winTexture->LoadTexture(m_graphics, "Assets//Textures//youwin.png");
	m_playagainTexture->LoadTexture(m_graphics, "Assets//Textures//buttonPlayAgain.png");

	// initialise sprites
	m_titleSprite->Init(m_titleTexture);
	m_buttonSprite->Init(m_buttonTexture);
	m_quitButtonSprite->Init(m_quitButtonTexture);
	m_matchSprite->Init(m_matchTexture);
	m_timesupSprite->Init(m_timesupTexture);
	m_winSprite->Init(m_winTexture);
	m_playagainSprite->Init(m_playagainTexture);

	// initialise title button
	m_titleButton = new Button();
	m_titleButton->Init(m_titleSprite, Vector2(m_graphics->GetWidth() * 0.2f, m_graphics->GetHeight() * 0.2f));
	m_titleButton->SetActive(true);

	// initialise timer button
	m_timerButton = new Button();
	m_timerButton->Init(m_buttonSprite, Vector2(m_graphics->GetWidth() * 0.2f, m_graphics->GetHeight() * 0.4f));
	m_timerButton->SetActive(true);

	// initialise counter button
	m_counterButton = new Button();
	m_counterButton->Init(m_buttonSprite, Vector2(m_graphics->GetWidth() * 0.2f, m_graphics->GetHeight() * 0.6f));
	m_counterButton->SetActive(true);

	// initialise quit button
	m_quitButton = new Button();
	m_quitButton->Init(m_quitButtonSprite, Vector2(m_graphics->GetWidth() * 0.2f, m_graphics->GetHeight() * 0.8f));
	m_quitButton->GetHitBox().OffSetAABB(m_quitButton->GetPosition() - m_quitButton->GetSprite()->GetOrigin()); // offset hitbox to correct position
	m_quitButton->SetActive(true);
	m_quitButton->SetID(L"QUIT"); // set ID
	m_clickables.push_back(m_quitButton); // add quit button to interactive object list

	// initialise play again button
	m_playagainButton = new Button;
	m_playagainButton->Init(m_playagainSprite, Vector2(middle.x, m_quitButton->GetPosition().y));
	m_playagainButton->GetHitBox().OffSetAABB(m_playagainButton->GetPosition() - m_playagainButton->GetSprite()->GetOrigin()); // off set hitbox to correct position
	m_playagainButton->SetID(L"PLAYAGAIN"); // set ID
	m_clickables.push_back(m_playagainButton); // add play again button to interactive object list

	// initialise match popup and add to notification list
	m_matchPopup = new Popup();
	m_matchPopup->Init(m_matchSprite, middle);
	m_matchPopup->SetID(L"MATCH"); // set ID
	m_popups.push_back(m_matchPopup);
	
	// initialise time's up popup and add to notification list
	m_timesupPopup = new Popup();
	m_timesupPopup->Init(m_timesupSprite, middle);
	m_timesupPopup->SetID(L"TIMESUP"); // set ID
	m_popups.push_back(m_timesupPopup);

	// initialise "you win" popup and add to notification list
	m_winPopup = new Popup();
	m_winPopup->Init(m_winSprite, middle);
	m_winPopup->SetID(L"YOUWIN"); // set ID
	m_popups.push_back(m_winPopup);
}

void 
Hud::Update(float deltaTime)
{
}

void 
Hud::Render()
{
	// render title button
	if(m_titleButton->IsActive())
	{
		m_titleButton->Render(m_graphics);
	}

	// render timer button
	if(m_timerButton->IsActive())
	{
		m_timerButton->Render(m_graphics);

		// temp string buffer for seconds
		std::wstring seconds = L"";
		if(m_timerSeconds < 10)
		{
			seconds = L"0" + std::to_wstring(m_timerSeconds);
		}
		else
		{
			seconds = std::to_wstring(m_timerSeconds);
		}

		// temp string buffer for minutes
		std::wstring minutes = L"";
		if(m_timerMinutes < 10)
		{
			minutes = L"0" + std::to_wstring(m_timerMinutes);
		}
		else
		{
			minutes = std::to_wstring(m_timerMinutes);
		}

		// concatenate timer string with seconds and minutes
		m_timerString = minutes + L":" + seconds;

		// render timer string in middle of timer button
		m_pixelmixFont->DrawString(m_graphics->GetSpriteBatch(),
								   m_timerString.c_str(),
								   m_timerButton->GetPosition(),
								   Colors::Red,
								   0.0f,
								   m_pixelmixFont->MeasureString(m_timerString.c_str()) / 2);
	}

	// render counter button
	if(m_counterButton->IsActive())
	{
		m_counterButton->Render(m_graphics);

		// make counter string using m_counter
		m_counterString = L"Matches left: " + std::to_wstring(m_counter);

		// render counter string
		m_pixelmixFont->DrawString(m_graphics->GetSpriteBatch(),
								   m_counterString.c_str(),
								   Vector2(m_counterButton->GetPosition().x, m_counterButton->GetPosition().y - 16),
								   Colors::Red,
								   0.0f,
								   m_pixelmixFont->MeasureString(m_counterString.c_str()) / 2,
								   0.8f);

		// make attempts string
		m_attemptsString = L"Attempts: " + std::to_wstring(m_attempts);

		// rended attempts string
		m_pixelmixFont->DrawString(m_graphics->GetSpriteBatch(),
								   m_attemptsString.c_str(),
								   Vector2(m_counterButton->GetPosition().x, m_counterButton->GetPosition().y + 16), 
								   Colors::Red,
								   0.0f,
								   m_pixelmixFont->MeasureString(m_attemptsString.c_str()) / 2,
								   0.8f);

	}

	// render interactive objects
	for(unsigned int i = 0; i < m_clickables.size(); i++)
	{
		if(m_clickables[i]->IsActive())
		{
			m_clickables[i]->Render(m_graphics);
		}
	}

	// render popups
	for(unsigned int i = 0; i < m_popups.size(); i++)
	{
		if(m_popups[i]->IsActive())
		{
			m_popups[i]->Render(m_graphics);
		}
	}
}

void 
Hud::ReleaseAll()
{
	// release all texture resources
	if(m_winTexture) { m_winTexture->Release(); }
	if(m_timesupTexture) { m_timesupTexture->Release(); }
	if(m_matchTexture) { m_matchTexture->Release(); }
	if(m_playagainTexture) { m_playagainTexture->Release(); }
	if(m_quitButtonTexture) { m_quitButtonTexture->Release(); }
	if(m_buttonTexture) { m_buttonTexture->Release(); }
	if(m_titleTexture) { m_titleTexture->Release(); }
}

void
Hud::DeleteAll()
{
	// clear popup list
	for(unsigned int i = 0; i < m_popups.size(); i++)
	{
		GameObject* temp = m_popups[i];
		delete temp;
		temp = nullptr;
	}
	m_popups.clear();

	// clear clickables list
	for(unsigned int i = 0; i < m_clickables.size(); i++)
	{
		GameObject* temp = m_clickables[i];
		delete temp;
		temp = nullptr;
	}
	m_clickables.clear();

	// delete timer button
	if(m_counterButton)
	{
		delete m_counterButton;
		m_counterButton = nullptr;
	}

	// delete timer button
	if(m_timerButton)
	{
		delete m_timerButton;
		m_timerButton = nullptr;
	}

	// delete title button111111111
	if(m_titleButton)
	{
		delete m_titleButton;
		m_titleButton = nullptr;
	}

	// delete win sprite
	if(m_winSprite)
	{
		delete m_winSprite;
		m_winSprite = nullptr;
	}

	// delete time's up sprite
	if(m_timesupSprite)
	{
		delete m_timesupSprite;
		m_timesupSprite = nullptr;
	}

	// delete play again sprite
	if(m_playagainSprite)
	{
		delete m_playagainSprite;
		m_playagainSprite = nullptr;
	}

	// delete quit button sprite
	if(m_quitButtonSprite)
	{
		delete m_quitButtonSprite;
		m_quitButtonSprite = nullptr;
	}

	// delete button sprite
	if(m_buttonSprite)
	{
		delete m_buttonSprite;
		m_buttonSprite = nullptr;
	}


	// delete win texture
	if(m_winTexture)
	{
		delete m_winTexture;
		m_winTexture = nullptr;
	}

	// delete time's up texture
	if(m_timesupTexture)
	{
		delete m_timesupTexture;
		m_timesupTexture = nullptr;
	}

	// delete title sprite
	if(m_titleSprite)
	{
		delete m_titleSprite;
		m_titleSprite = nullptr;
	}
	
	// delete match notification texture
	if(m_matchTexture)
	{
		delete m_matchTexture;
		m_matchTexture = nullptr;
	}

	// delete play again button texture
	if(m_playagainTexture)
	{
		delete m_playagainTexture;
		m_playagainTexture = nullptr;
	}
	
	// delete quit button texture
	if(m_quitButtonTexture)
	{
		delete m_quitButtonTexture;
		m_quitButtonTexture = nullptr;
	}

	// delete button texture
	if(m_buttonTexture)
	{
		delete m_buttonTexture;
		m_buttonTexture = nullptr;
	}

	// delete title texture
	if(m_titleTexture)
	{
		delete m_titleTexture;
		m_titleTexture = nullptr;
	}

	// delete spritefont
	if(m_pixelmixFont)
	{
		delete m_pixelmixFont;
		m_pixelmixFont = nullptr;
	}
}

void
Hud::Reset()
{
	// turn off play again button
	m_playagainButton->SetActive(false);

	// turn off all popup notifications
	for(unsigned int i = 0; i < m_popups.size(); i++)
	{
		m_popups[i]->SetActive(false);
	}
}

void
Hud::SetTimer(unsigned int minutes, unsigned int seconds)
{
	// copy timer values to hud timer variables
	m_timerMinutes = minutes;
	m_timerSeconds = seconds;
}

void
Hud::SetCounter(unsigned int counter)
{
	m_counter = counter;
}

void 
Hud::SetAttempts(unsigned int attempts)
{
	m_attempts = attempts; // copy attempts to private member
}

void 
Hud::TogglePopup(const wchar_t* ID, bool active)
{
	// loop through poups to find the match notification
	for(unsigned int i = 0; i < m_popups.size(); i++)
	{
		if(m_popups[i]->GetID() == ID)
		{
			// toggle active setting
			m_popups[i]->SetActive(active);
			break;
		}
	}
}

void Hud::ShowPlayAgain()
{	
	// show play again button
	m_playagainButton->SetActive(true);
}
