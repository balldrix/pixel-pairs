// Popup.h
// Christopher Ball 2016
// Class object for notification popups

#ifndef _POPUP_H_
#define _POPUP_H_

#include "GameObject.h"

// forward declarations
class Graphics;

class Popup : public GameObject
{
public:
	Popup();
	~Popup();
	void Update(float deltaTime); // update popup
	void Render(Graphics* graphics); // render popup

private:
	// no private members needed
};

#endif _POPUP_H_
