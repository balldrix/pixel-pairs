#include "Card.h"
#include "Sprite.h"

Card::Card() :
m_cardFace(MAX_FACES),
m_matched(false)
{}

Card::~Card()
{}

void
Card::Update(float deltaTime)
{
}

void
Card::Render(Graphics* graphics)
{
	// set sprite position and render
	m_sprite->SetPosition(m_position);
	m_sprite->Render(graphics);
}

void 
Card::SetFace(CardFace face)
{
	m_cardFace = face;
}

void 
Card::SetMatched(bool matched)
{
	m_matched = matched;
}
