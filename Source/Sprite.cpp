#include "Sprite.h"

#include "Graphics.h"
#include "Texture.h"

Sprite::Sprite() :
m_texture(nullptr),
m_position(0.0f, 0.0f),
m_scale(0.0f),
m_rotation(0.0f),
m_alpha(0.0f),
m_colour(Colors::White.v),
m_origin(0.0f, 0.0f),
m_rect(),
m_width(0),
m_height(0),
m_active(false)
{
}

Sprite::~Sprite()
{
}

void 
Sprite::Init(Texture* texture)
{
	// initialise texture
	m_texture = texture;
	
	// copy D3D11 resource to a Texture2D struct
	ID3D11Texture2D*	tex2D;
	tex2D = (ID3D11Texture2D*)m_texture->GetResource();

	// create a texture 2D description
	// and get data from the Texture2D resource
	D3D11_TEXTURE2D_DESC desc;
	tex2D->GetDesc(&desc);
	tex2D->Release();

	// set dimensions
	m_width = desc.Width;
	m_height = desc.Height;

	// set source rect
	m_rect.top = (LONG)0.0f;
	m_rect.left = (LONG)0.0f;
	m_rect.right = (LONG)m_width;
	m_rect.bottom = (LONG)m_height;

	// set origin
	m_origin.x = m_width / 2.0f;
	m_origin.y = m_height / 2.0f;

	// set standard scale and alpha
	m_scale = 1.0f;
	m_alpha = 1.0f;
}

void 
Sprite::Render(Graphics* graphics)
{
	// set alpha colour key
	m_colour.w = m_alpha;

	graphics->GetSpriteBatch()->Draw(m_texture->GetTexture(), 
						m_position,
						&m_rect,
						m_colour,
						m_rotation,
						m_origin,
						m_scale,
						SpriteEffects::SpriteEffects_None,
						0.0f);
}

void 
Sprite::Render(Graphics* graphics, Vector2 position)
{
	// set alpha colour key
	m_colour.w = m_alpha;

	// draw sprite
	graphics->GetSpriteBatch()->Draw(m_texture->GetTexture(), 
									 position,
									 &m_rect,
									 m_colour,
									 m_rotation,
									 m_origin,
									 m_scale,
									 SpriteEffects::SpriteEffects_None,
									 0.0f);
}

void 
Sprite::SetPosition(const Vector2& position)
{
	m_position = position;
}

void 
Sprite::SetScale(const float& scale)
{
	m_scale = scale;
}

void 
Sprite::SetRotation(const float& rotation)
{
	m_rotation = rotation;
}

void 
Sprite::SetAlpha(const float& alpha)
{
	m_alpha = alpha;
}

void
Sprite::SetColour(const Color& colour)
{
	m_colour = colour;
}

void
Sprite::SetOrigin(const Vector2& origin)
{
	m_origin = origin;
}

void 
Sprite::SetRect(const RECT& rect)
{
	m_rect = rect;
}

void 
Sprite::SetActive(bool active)
{
	m_active = active;
}
